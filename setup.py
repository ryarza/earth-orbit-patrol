import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="eorbpatrol",
    version="0.0.1",
    author="Jackson Barnes, Luis Paternina, Ricardo Yarza",
    author_email="ryarza@ucsc.edu",
    description="A Python package to monitor patches of the sky for intrusive objects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ryarza/earth-orbit-patrol",
    project_urls={
        "Bug Tracker": "https://gitlab.com/ryarza/earth-orbit-patrol/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPL v3 or later",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "."},
    packages=setuptools.find_packages(where="."),
    python_requires=">=3.6",
)
