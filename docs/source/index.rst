.. earth-orbit-patrol documentation master file, created by
   sphinx-quickstart on Fri Jun 25 11:21:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to earth-orbit-patrol's documentation!
==============================================

A Python package to monitor patches of the sky for intrusive objects.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   obs
   io
   vis
   utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
