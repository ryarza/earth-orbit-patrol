# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

def visualization(F,normalize=True,plot_field=True):
    """Visualization
    This function plots a two-variable function over a sphere

    Args:
        F (array): numpy 2d array. An matrix that refers to the field 
            that is going to be plotted on the surface
        normalize (bool): flag variable to define if the F field
            is going to be normalized
        plot_field (bool): flag variable to define if the F field
            is going to be plotted
    
    Return:
        None
    """
    numpoints = F.shape[0]

    phi = np.linspace(0, np.pi, numpoints)
    theta = np.linspace(0, 2*np.pi, numpoints)
    phi, theta = np.meshgrid(phi, theta)

    # The Cartesian coordinates of the unit sphere
    x = np.sin(phi) * np.cos(theta)
    y = np.sin(phi) * np.sin(theta)
    z = np.cos(phi)

    if normalize:
        # Normalize the values of the field to [0,1]
        fmax = F.max()
        fmin = F.min()
        F = (F - fmin)/(fmax - fmin)
    #End if

    # Set the aspect ratio to 1 so our sphere looks spherical
    fig = plt.figure(figsize=plt.figaspect(1.))
    ax = fig.add_subplot(111, projection='3d')
    if plot_field:
        # ax.plot_surface(x,y,z,rstride=1,cstride=1,facecolors=cm.jet(F))
        ax.plot_surface(x,y,z,rstride=1,cstride=1,facecolors=cm.viridis(F))
    else:
        ax.plot_surface(x,y,z)
    # End if

    # Turn off the axis planes
    ax.set_axis_off()
    plt.show()
# End function
