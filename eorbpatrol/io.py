# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import tletools; from tletools import TLE; from tletools.pandas import load_dataframe
import rebound

def read_data(input_data):
  sim = starlink(input_data)
  sim.N_active = sim.N
  sim.move_to_com()
  return sim

"""
Reading in the raw data from a TLE file into the REBOUND N-body simulation
"""
def starlink(file_input):
  sim = rebound.Simulation()
  sim.units = ("hr", "km", "kg")
  
  ## First add in the Earth ##
  sim.add("Earth")
  
  sl_df = load_dataframe(file_input)
  
  sl_df["inc"] = np.radians(sl_df["inc"])
  sl_df["raan"] = np.radians(sl_df["raan"])
  sl_df["argp"] = np.radians(sl_df["argp"])
  sl_df["M"] = np.radians(sl_df["M"])

  sl_df["period"] = 24/sl_df["n"]
  
  for sat in range(len(sl_df)):
    sim.add(primary = sim.particles[0], inc = sl_df["inc"].iloc[sat], e = sl_df["ecc"].iloc[sat], Omega = sl_df["raan"].iloc[sat], omega = sl_df["argp"].iloc[sat], M = sl_df["M"].iloc[sat], P = sl_df["period"].iloc[sat])
  return sim

