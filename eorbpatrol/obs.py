# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import rebound
from . import io
from . import utils

class Observation():
  """Properties of an observation independent from the objects in orbit.

  :param t: Initial and final observation times, respectively.
  :type t: numpy.ndarray

  :param theta: Minimum and maximum polar angles of interest, respectively.
  :type theta: numpy.ndarray

  :param phi: Minimum and maximum azimuthal of interest, respectively.
  :type phi: numpy.ndarray

  """

  def __init__(self, t, theta, phi):
    self.phi = phi
    self.theta = theta
    self.t = t
    self.__N_obstructors = None
    self.objects = None

  def compute(self, input_data, data_time):
    """Evolves a Rebound simulation up until the end of the observation.

    :param input_data: Path of file containing sizes, positions, and velocities of objects in orbit.
    :type input_data: str

    :param data_time: Time to which the data corresponds. The data will be evolved form this time to the end of the observation.
    :type data_time: float

    """

    assert data_time <= self.t[0], "Can't integrate backwards yet!"
    sim = io.read_data(input_data)

#   Divide observation in, say, 100 steps, and save satellite position at these points in time. A good approximation if self.t[1] - input_data <~ the dynamical time of the orbits
    self.times = np.linspace(0.,self.t[1] - data_time, 200)
#   Will store positions of all objects at all times in spherical coordinates
    self.objects = np.empty((200, sim.N, 3))
    for i, time in enumerate(self.times):
      sim.integrate(time)
      ps = sim.particles
      for p in range(sim.N):
        self.objects[i, p, 0] = ps[p].x
        self.objects[i, p, 1] = ps[p].y
        self.objects[i, p, 2] = ps[p].z
        
#   Convert to spherical coordinates
    for i in range(self.objects.shape[0]):
      for j in range(self.objects.shape[1]):
        self.objects[i, j] = utils.cartesian_to_spherical(self.objects[i, j])
#   Reset obstructors
    self.__N_obstructors = None

  @property
  def N_obstructors(self):
    """

    :return: Number of objects passing through the sky patch defined by the observation.
    :rtype: int

    """
    if self.__N_obstructors is None:
      assert self.objects != None, "You need to provide input data and evolve them first!"
      self.__N_obstructors = 0
#   Loop over objects, check if they lie in the patch. If they do, add 1 to the count
      for i in range(len(self.objects)):
        if ( self.objects[:, i, 1] > self.theta[0] ) and ( self.objects[:, i, 1] < self.theta[1] ) and ( self.objects[:, i, 2] > self.phi[0] ) and ( self.objects[:, i, 2] < self.phi[1] ):
          self.__N_obstructors += 1
    return self.__N_obstructors
