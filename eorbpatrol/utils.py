# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

def cartesian_to_spherical(x):
  coords = np.empty(3)
  coords[0] = np.linalg.norm(x)
  coords[1] = np.arccos(x[2] / coords[0])
  coords[2] = np.arctan2(x[1], x[0])
  return coords
