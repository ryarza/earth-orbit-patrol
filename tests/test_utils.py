# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pytest

import eorbpatrol.utils as utils

# One in each quadrant
def test_cartesian_to_spherical():
  """ Conversion from cartesian to spherical coordinates
  """
  x = np.array([0.6172268938,0.8083743944,0.098126504,0.6805937112,-0.9553985831,-0.6738918118,-0.1055651875,-0.7168139498])
  y = np.array([0.5216945731,0.7045100057,-0.8746355841,-0.9528827481,0.8731046239,0.9977935021,-0.2816669576,-0.4281531608])
  z = np.array([0.335710145,-0.5350340172,0.402438141,-0.4975168137,0.4431591516,-0.9831386329,0.8298339347,-0.6869281562])

  coords_cartesian = np.array([x,y,z]).T

  coords_spherical = np.empty_like(coords_cartesian)
  for idx, coords in enumerate(coords_cartesian):
    coords_spherical[idx] = utils.cartesian_to_spherical(coords)

  correct_coords_spherical = np.array([[0.8751203160540791, 1.1770876424968075, 0.7017144187393334], [1.1983592571771498, 2.0336151305083985, 0.7168524976054825], [0.9677668484662919, 1.1419278274338274, -1.4590722157536424], [1.2722878256095975, 1.9725588049279952, -0.9505724298431454], [1.3680234539903022, 1.2409039669363666, 2.4011703275996386], [1.5544399693938384, 2.2555358570147357, 2.1648084053406333], [0.8826690450081998, 0.3477505922372535, -1.9293804728121993], [1.0812065758404448, 2.259238306486391, -2.6031606185643996]])

  for i in range(coords_cartesian.shape[0]):
    for j in range(3):
      assert coords_spherical[i,j] == pytest.approx(correct_coords_spherical[i, j], rel=1e-10)
