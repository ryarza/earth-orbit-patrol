import numpy as np
import matplotlib.pyplot as plt

import eorbpatrol.vis as vis

#def test_visualization_routine():
#    """
#    This funcion tests if the visualization() function works correctly
#    Args:
#        None
#    Return:
#        None
#    """
#
#    numpoints = 100
#    Lx = 1.0
#    Ly = 1.0
#    Cx = 0.2
#    Cy = 0.7
#    Dx = 0.1
#    Dy = 0.1
#
#    x = np.linspace(0,Lx,numpoints)
#    y = np.linspace(0,Ly,numpoints)
#    X_f,Y_f = np.meshgrid(x,y)
#
#    bigaussian_field = np.exp(-(X_f - Cx)**2/(2*Dx**2))*np.exp(-(Y_f - Cy)**2/(2*Dy**2))
#    random_field = field = np.random.rand(numpoints,numpoints)
#
#    total_field = bigaussian_field + random_field
#
#    # Code suggested in 
#    # https://towardsdatascience.com/unit-testing-python-data-visualizations-18e0250430
#    num_figure_before = plt.gcf().number
#    vis.visualization(total_field)
#    num_figure_after = plt.gcf().number
#    assert num_figure_before < num_figure_after
## End function
#
#if __name__ == '__main__':
#    test_visualization_routine()
