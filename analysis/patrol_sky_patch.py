# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt
import eorbpatrol

#Define an observation
obs = eorbpatrol.obs.Observation([0, 1], [np.pi / 4, np.pi / 2], [np.pi / 4, np.pi / 2])
#Evolve objects
obs.compute('../eorbpatrol/starlink_raw.txt', 0)

# Make frames for animation
for i in range(obs.objects.shape[0]):
  fig, ax = plt.subplots()
  ax.scatter(obs.objects[i,:,1], obs.objects[i,:,2])
  ax.set_xlim(obs.theta[0], obs.theta[1])
  ax.set_ylim(obs.phi[0], obs.phi[1])
  ax.set_title(obs.times[i])
  plt.savefig('../../figures/patches/patch_' + str(i) + '.png')
  plt.close()
