# Copyright 2021 Jackson Barnes, Luis Paternina, Ricardo Yarza

# This file is part of earth-orbit-patrol.

# earth-orbit-patrol is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# earth-orbit-patrol is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with earth-orbit-patrol.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt

import eorbpatrol
import eorbpatrol.utils

#Load data into rebound simulation
sim = eorbpatrol.io.read_data('../eorbpatrol/starlink_raw.txt')
ps = sim.particles

#Save x, y, and z values
x_vals = np.empty(sim.N)
y_vals = np.empty(sim.N)
z_vals = np.empty(sim.N)
for i in range(sim.N):
  x_vals[i] = sim.particles[i].x
  y_vals[i] = sim.particles[i].y
  z_vals[i] = sim.particles[i].z

#Plot objects in 3D
fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(x_vals, y_vals, z_vals)
ax.set_xlabel('x [km]')
ax.set_ylabel('y [km]')
ax.set_zlabel('z [km]')
plt.show()

#Plot theta-phi distribution
cartesian = np.array([x_vals, y_vals, z_vals]).T
spherical = np.empty_like(cartesian)
for i in range(cartesian.shape[0]):
  spherical[i] = eorbpatrol.utils.cartesian_to_spherical(cartesian[i])

spherical = spherical.T
theta = spherical[1]
phi = spherical[2]

fig, ax = plt.subplots()
ax.scatter(theta, phi)
ax.set_xlabel(r'$\theta$ [rad]')
ax.set_ylabel(r'$\phi$ [rad]')
plt.show()
plt.close()
